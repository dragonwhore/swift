// Создать класс родитель и 2 класса наследника
class ParentClass {
    var property1: String
    var property2: Int
    
    init(property1: String, property2: Int) {
        self.property1 = property1
        self.property2 = property2
    }
    
    func method1() {
        print("This is a method of ParentClass")
    }
}

class ChildClass1: ParentClass {
    var property3: Bool
    
    init(property1: String, property2: Int, property3: Bool) {
        self.property3 = property3
        super.init(property1: property1, property2: property2)
    }
    
    override func method1() {
        super.method1()
        print("This is a method of ChildClass1")
    }
}

class ChildClass2: ParentClass {
    var property4: Double
    
    init(property1: String, property2: Int, property4: Double) {
        self.property4 = property4
        super.init(property1: property1, property2: property2)
    }
    
    override func method1() {
        super.method1()
        print("This is a method of ChildClass2")
    }
}

let parent = ParentClass(property1: "Hello", property2: 42)
let child1 = ChildClass1(property1: "Hi", property2: 27, property3: true)
let child2 = ChildClass2(property1: "Hey", property2: 99, property4: 3.14)

parent.method1() // This is a method of ParentClass
child1.method1() // This is a method of ParentClass, This is a method of ChildClass1
child2.method1() // This is a method of ParentClass, This is a method of ChildClass2

// Создать класс *House* в нем несколько свойств - *width*, *height* и несколько методов: *create*(выводит площадь),*destroy*(отображает что дом уничтожен)
class House {
    var width: Double
    var height: Double

    init(width: Double, height: Double) {
        self.width = width
        self.height = height
    }

    func create() {
        let area = width * height
        print("House created with width: \(width), height: \(height) and area: \(area)")
    }

    func destroy() {
        print("The house with width: \(width) and height: \(height) has been destroyed")
    }
}
// Пример использования класса:
let house = House(width: 10, height: 5)
house.create()
house.destroy()

// Создайте класс с методами, которые сортируют массив учеников по разным параметрам
class Student {
    var name: String
    var age: Int
    var grade: Int

    init(name: String, age: Int, grade: Int) {
        self.name = name
        self.age = age
        self.grade = grade
    }
}

class StudentSorter {
    func sortByName(_ students: [Student]) -> [Student] {
        return students.sorted(by: { $0.name < $1.name })
    }

    func sortByAge(_ students: [Student]) -> [Student] {
        return students.sorted(by: { $0.age < $1.age })
    }

    func sortByGrade(_ students: [Student]) -> [Student] {
        return students.sorted(by: { $0.grade > $1.grade })
    }
}
// Пример использования:
let student1 = Student(name: "John", age: 15, grade: 90)
let student2 = Student(name: "Jane", age: 16, grade: 80)
let student3 = Student(name: "Bob", age: 14, grade: 95)

let students = [student1, student2, student3]

let sorter = StudentSorter()
let sortedByName = sorter.sortByName(students)
let sortedByAge = sorter.sortByAge(students)
let sortedByGrade = sorter.sortByGrade(students)

print("Students sorted by name:")
for student in sortedByName {
    print(student.name)
}

print("\nStudents sorted by age:")
for student in sortedByAge {
    print("\(student.name) - \(student.age) years old")
}

print("\nStudents sorted by grade:")
for student in sortedByGrade {
    print("\(student.name) - Grade: \(student.grade)")
}

// Написать свою структуру и класс, и пояснить в комментариях - чем отличаются структуры от классов
// Структура студента
struct Student {
    var name: String
    var age: Int
}

// Класс студента
class StudentClass {
    var name: String
    var age: Int
    
    init(name: String, age: Int) {
        self.name = name
        self.age = age
    }
}
// Оба объекта имеют одни и те же свойства, но есть несколько ключевых различий:
// Структуры создаются с помощью ключевого слова struct, а классы - с помощью ключевого слова class.
// Структуры передаются по значению, а классы - по ссылке.
// Классы имеют методы, которые могут изменять свои свойства и состояние объекта, а структуры не могут.
// Классы могут наследовать другие классы, а структуры не могут.

//  Напишите простую программу, которая отбирает комбинации в покере из рандомно выбранных 5 карт
//  Сохраняйте комбинации в массив
//  Если выпала определённая комбинация - выводим соответствующую запись в консоль
//  Результат в консоли примерно такой: 'У вас бубновый стрит флеш'.

// Создадим класс PokerHand:
class PokerHand {
    var cards: [String]

    init(cards: [String]) {
        self.cards = cards
    }

    func checkCombination() -> String {
        // код проверки комбинаций
    }
}
// Теперь нужно реализовать метод checkCombination(), который будет проверять комбинации на соответствие:
func checkCombination() -> String {
    let suits = cards.map { $0.suffix(1) } // масти всех карт
    let ranks = cards.map { $0.prefix(1) } // номиналы всех карт
    let uniqueRanks = Set(ranks) // уникальные номиналы

    // проверка на роял-флеш
    if Set(suits) == Set(["♠️", "♥️", "♣️", "♦️"]) && uniqueRanks == Set(["A", "K", "Q", "J", "10"]) {
        return "Роял-флеш"
    }

    // проверка на стрит-флеш
    if Set(suits).count == 1 && uniqueRanks.count == 5 && (uniqueRanks.max()!.asciiValue! - uniqueRanks.min()!.asciiValue!) == 4 {
        return "

