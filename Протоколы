// Реализовать структуру IOSCollection и создать в ней copy on write по типу - Видео-урок
struct IOSCollection<Element>: RandomAccessCollection {
    private var storage: Storage
    
    var startIndex: Int { storage.startIndex }
    var endIndex: Int { storage.endIndex }
    
    subscript(index: Int) -> Element {
        get { storage[index] }
        set {
            if !isKnownUniquelyReferenced(&storage) {
                storage = Storage(storage)
            }
            storage[index] = newValue
        }
    }
    
    init(_ elements: [Element] = []) {
        storage = Storage(elements)
    }
    
    private final class Storage {
        var elements: [Element]
        
        init(_ elements: [Element]) {
            self.elements = elements
        }
        
        subscript(index: Int) -> Element {
            get { elements[index] }
            set { elements[index] = newValue }
        }
        
        var startIndex: Int { elements.startIndex }
        var endIndex: Int { elements.endIndex }
    }
}

// Создать протокол *Hotel* с инициализатором, который принимает roomCount, после создать class HotelAlfa добавить свойство roomCount и подписаться на этот протокол
protocol Hotel {
    init(roomCount: Int)
}

class HotelAlfa: Hotel {
    var roomCount: Int
    
    required init(roomCount: Int) {
        self.roomCount = roomCount
    }
    
    // Другие свойства и методы
}

// Создать protocol GameDice у него {get} свойство numberDice далее нужно расширить Int так, чтобы когда мы напишем такую конструкцию 'let diceCoub = 4 diceCoub.numberDice' в консоле мы увидели такую строку - 'Выпало 4 на кубике'
protocol GameDice {
    var numberDice: Int { get }
}

extension Int: GameDice {
    var numberDice: Int {
        print("Выпало \(self) на кубике")
        return self
    }
}

// Создать протокол с одним методом и 2 свойствами одно из них сделать явно optional, создать класс, подписать на протокол и реализовать только 1 обязательное свойство
class MyClass: MyProtocol {
    var requiredProperty: String
    
    init(requiredProperty: String) {
        self.requiredProperty = requiredProperty
    }
    
    func myMethod() {
        // Реализация метода
    }
}


// Создать 2 протокола: со свойствами время, количество кода и функцией writeCode(platform: Platform, numberOfSpecialist: Int); и другой с функцией: stopCoding(). Создайте класс: Компания, у которого есть свойства - количество программистов, специализации(ios, android, web)
// Компании подключаем два этих протокола
// Задача: вывести в консоль сообщения - 'разработка началась. пишем код <такой-то>' и 'работа закончена. Сдаю в тестирование', попробуйте обработать крайние случаи.
protocol Coding {
    var time: Int { get }
    var linesOfCode: Int { get }
    func writeCode(platform: Platform, numberOfSpecialists: Int)
}

protocol CodingControl {
    func stopCoding()
}

class Company: Coding, CodingControl {
    var numberOfProgrammers: Int
    var specializations: [Specialization]
    var time: Int = 0
    var linesOfCode: Int = 0
    
    init(numberOfProgrammers: Int, specializations: [Specialization]) {
        self.numberOfProgrammers = numberOfProgrammers
        self.specializations = specializations
    }
    
    func writeCode(platform: Platform, numberOfSpecialists: Int) {
        print("Разработка началась. Пишем код для \(platform) с помощью \(numberOfSpecialists) специалистов")
        // Здесь можно добавить код, который будет писать код
        time += 1
        linesOfCode += 1000
    }
    
    func stopCoding() {
        print("Работа закончена. Сдаю в тестирование")
        // Здесь можно добавить код, который будет останавливать процесс разработки
    }
}

enum Specialization {
    case iOS
    case Android
    case Web
}

enum Platform {
    case iOS
    case Android
    case Web
}
